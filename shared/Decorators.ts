import { createDecorator } from "vue-class-component";

export const Watch = createDecorator((options, key) => {
    if (options.watch == null)
        options.watch = {};
    if (options.methods == null)
        return;
    options.watch[key.replace(/^watch_/, "")] = options.methods[key];
})