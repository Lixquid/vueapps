declare module "save-svg-as-png" {
    const saveSvg: (el: HTMLElement, filename: string) => void;
    const saveSvgAsPng: (el: HTMLElement, filename: string) => void;
}